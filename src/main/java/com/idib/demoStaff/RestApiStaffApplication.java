package com.idib.demoStaff;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestApiStaffApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestApiStaffApplication.class, args);
	}

}
