package com.idib.demoStaff.DAO;

import com.idib.demoStaff.Config.HibernateConfig;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.List;

public class EntityDAO<Bean> {
    private final Class<Bean> typeParameterClass;

    private final SessionFactory sessionFactory;

    private EntityDAO(Class<Bean> typeParameterClass, SessionFactory sessionFactory) {
        this.typeParameterClass = typeParameterClass;
        this.sessionFactory = sessionFactory;
    }

    public static EntityDAO<Bean> getDAOFor(Class<Bean> typeParameterClass) {
        ApplicationContext context = new AnnotationConfigApplicationContext(HibernateConfig.class);
        return (EntityDAO<Bean>) context.getBean(EntityDAO.class, typeParameterClass);
    }

    private Session getSession() {
        return this.sessionFactory.getCurrentSession();
    }

    public void delete(int id) {
        Session session = getSession();
        session.beginTransaction();
        Bean del = (Bean) session.get(typeParameterClass, id);
        session.delete(del);
        session.getTransaction().commit();
        if (session.isOpen()) {
            session.close();
        }
    }

    public List<Bean> getAll() {
        Session session = getSession();
        session.beginTransaction();
        String hql = String.format("from %s", typeParameterClass.getCanonicalName());
        Query SQLQuery = session.createQuery(hql);
        List<Bean> result = SQLQuery.list();
        session.getTransaction().commit();
        if (session.isOpen()) {
            session.close();
        }
        return result;
    }

    public Bean getById(int id) {
        Session session = getSession();
        session.beginTransaction();
        Bean result = (Bean) session.get(typeParameterClass, id);
        session.getTransaction().commit();
        if (session.isOpen()) {
            session.close();
        }
        return result;
    }

    public void update(Bean object) {
        Session session = getSession();
        session.beginTransaction();
        session.update(object);
        session.getTransaction().commit();
        if (session.isOpen()) {
            session.close();
        }
    }

    public void add(Bean object) {
        Session session = getSession();
        session.beginTransaction();
        session.save(object);
        session.getTransaction().commit();
        if (session.isOpen()) {
            session.close();
        }
    }

    public void clear() {
        Session session = getSession();
        session.beginTransaction();
        String hql = String.format("delete from %s", typeParameterClass.getCanonicalName());
        Query query = session.createQuery(hql);
        query.executeUpdate();
        session.getTransaction().commit();
        if (session.isOpen()) {
            session.close();
        }
    }
}