package com.idib.demoStaff.TDO;

public class DepartmentInfoDTO {
    private int id;
    private String name;
    private int countStaffOnWork;
    private int countStaffAbsence;
    private int countStaffBeIll;

    public DepartmentInfoDTO(int id, String name, int countStaffOnWork, int countStaffAbsence, int countStaffBeIll) {
        this.id = id;
        this.name = name;
        this.countStaffOnWork = countStaffOnWork;
        this.countStaffAbsence = countStaffAbsence;
        this.countStaffBeIll = countStaffBeIll;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCountStaffOnWork() {
        return countStaffOnWork;
    }

    public void setCountStaffOnWork(int countStaffOnWork) {
        this.countStaffOnWork = countStaffOnWork;
    }

    public int getCountStaffAbsence() {
        return countStaffAbsence;
    }

    public void setCountStaffAbsence(int countStaffAbsence) {
        this.countStaffAbsence = countStaffAbsence;
    }

    public int getCountStaffBeIll() {
        return countStaffBeIll;
    }

    public void setCountStaffBeIll(int countStaffBeIll) {
        this.countStaffBeIll = countStaffBeIll;
    }
}