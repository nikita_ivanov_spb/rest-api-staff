package com.idib.demoStaff.TDO;

public class PairDTO<S,T> {
    public S first;
    public T second;

    public PairDTO(S first, T second) {
        this.first = first;
        this.second = second;
    }

    public S getFirst() {
        return first;
    }

    public void setFirst(S first) {
        this.first = first;
    }

    public T getSecond() {
        return second;
    }

    public void setSecond(T second) {
        this.second = second;
    }
}
