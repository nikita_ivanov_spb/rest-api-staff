package com.idib.demoStaff.controller;

import com.idib.demoStaff.util.DBConnector;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/dep")
public class DepartmentController {
    @RequestMapping(value = "/{id}/countWorkHours", method = RequestMethod.GET)
    public Map getCountWorkHoursById(@PathVariable int id) {
        int countWorkDay = DBConnector.CalculateWorkDayStaffByDeaId(id);
        long countHours = countWorkDay * 8;
        return Collections.singletonMap("countWorkHours", countHours);
    }

    @RequestMapping(value = "/deps/info", method = RequestMethod.GET)
    public List getInfo() {
        return DBConnector.getAggregateInfo();
    }
}
