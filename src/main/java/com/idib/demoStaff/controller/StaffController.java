package com.idib.demoStaff.controller;

import com.idib.demoStaff.util.DBConnector;
import com.idib.demoStaff.TDO.PairDTO;
import com.idib.demoStaff.entity.StaffEntity;
import com.idib.demoStaff.util.DateUtil;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Date;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/staff")
public class StaffController {

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public StaffEntity getById(@PathVariable int id) {
        Optional<StaffEntity> optStaff = DBConnector.getStaffById(id);
        return optStaff.get();
    }

    @RequestMapping(value = "/{id}/countWorkDay", method = RequestMethod.GET)
    public Map getCountWorkDayById(@PathVariable int id) {
        int countWorkDay = DBConnector.CalculateWorkDayStaffById(id);
        return Collections.singletonMap("countWorkDay", countWorkDay);
    }

    @RequestMapping(value = "/{id}/lastHoliday", method = RequestMethod.GET)
    public Map getLastHolidayById(@PathVariable int id) {
        Date date = DBConnector.getLastHolidayStaffById(id);


        return Collections.singletonMap("lastWeekend", date);
    }

    @RequestMapping(value = "/{id}/birthday", method = RequestMethod.GET)
    public Map getBirthdayById(@PathVariable int id) {
        Optional<StaffEntity> optStaff = DBConnector.getStaffById(id);
        return Collections.singletonMap("birthday", optStaff.get().getBirthday());
    }

    @RequestMapping(value = "/birthdays", method = RequestMethod.GET)
    public List getBirthdays() {
        List<StaffEntity> staffEntities = DBConnector.getStaffs();
        List res = staffEntities.stream()
                .map(x -> new PairDTO(x.getBirthday(), x.getId()))
                .collect(Collectors.toList());
        return res;
    }

    @RequestMapping(value = "/{id}/birthdays", method = RequestMethod.POST)
    public ResponseEntity<String> mark(@PathVariable int id) {
        Date sqlDate = null;
        boolean isWeekend = DateUtil.isWeekend(sqlDate);
        int countWorker = 0;
        if (countWorker > 0 && isWeekend) {
            return new ResponseEntity<>("Mark save", HttpStatus.OK);
        }

        String errorText = "";
        if (countWorker < 1)
            errorText += "One or more worker should be in office\n";

        if (!isWeekend)
            errorText += "People should not work in weekend\n";

        return new ResponseEntity<>(errorText, HttpStatus.CONFLICT);

    }
}
